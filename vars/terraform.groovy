def clone(String branch_name,String Url) {
  echo "$branch_name"
  echo "$Url"
  git branch: 'main', url: 'https://gitlab.com/nhsinha235/sonarqube.git'
  
}
def installterraform (){
sh 'docker build -t opstree/tf:1 .'
sh 'docker run  --rm  -v $(pwd):/workdir opstree/tf:1 init'

}
def validate(){
sh 'docker run  --rm -v $(pwd):/workdir opstree/tf:1 validate'
}
def plan (){
sh 'docker run  --rm -v ~/.aws:/root/.aws -v $(pwd):/workdir opstree/tf:1  plan'
}


def apply (){
sh 'docker run  --rm -v ~/.aws:/root/.aws -v $(pwd):/workdir opstree/tf:1  apply -auto-approve'
}
def destroy (){
sh 'docker run --rm  -v ~/.aws:/root/.aws -v ${HOME}/.ssh:/root/.ssh --rm -v ${PWD}:/src opstree/tf:1 destroy -auto-approve'
}

